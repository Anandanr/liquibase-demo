#!/bin/bash
#########################################################################################
# Author: Sumitabha Chandra
# Date: 24/03/2020
# Instruction: initiate only from project's root directory, from command prompt/terminal
##########################################################################################


# Please don't change below if you are not sure what are you doing
# ########################################################################################

# docker compose file location(configurable)
readonly DOCKER_COMPOSE_FILE="./docker/docker-compose-mysql-db.yml"

function checkSystem(){
	notready=false

	echo -e "Checking required dependencis..."

	which docker &> /dev/null
	if [[ $? -eq 0 ]]; then
		echo "[OK]: docker is installed..."
	else
		echo "[FAILED]: docker is not installed, please install docker"
		notready=true
	fi

	which docker-compose &> /dev/null
	if [[ $? -eq 0 ]]; then
		echo "[OK]: docker-compose is installed..."
	else
		echo "[FAILED]: docker-compose is not installed, please install docker-compose"
		notready=true
	fi

	if [[ notready == "true" ]]; then
		echo "Please install required software modules , then run script again"
		exit 1
	fi
	echo -e "+-+-+-+-+-+\n|M|y|S|Q|L|\n+-+-+-+-+-+"
		 
}

function statusMySQLContainer(){
	# down if there is any existing docker compose ps
	docker-compose -f ${DOCKER_COMPOSE_FILE} ps 
	echo
}

function stopMySQLContainer(){
	# down if there is any existing docker compose ps
	docker-compose -f ${DOCKER_COMPOSE_FILE} down 
	echo
}

function startMySQLContainer(){
	# Run mysql docker in detached mode( -d)
	docker-compose -f ${DOCKER_COMPOSE_FILE} up -d
	echo
}

function showHelpMsg(){
	echo -e "Usage: ./mysql-container [-u|-up ; -d|--down ; -r|--restart ; -s|--status ; -h|--help]\n\
Valid options:\n \
no arguments \t\t :Starts container\n \
-u | --up \t\t :Starts container\n \
-d | --down \t\t :Stops container\n \
-r | --restart \t :Restarts container\n \
-s | --status \t\t :Display container status\n \
-h | --help \t\t :Print this messages";
	echo
}

function main(){
	checkSystem
	local arguments=$#;
	
	if [[ ${arguments} == 0 ]]; then
		echo -e "No arguments provided...";
		startMySQLContainer;

	elif [[ 1 < ${arguments} ]]; then
		echo -e "arguments : ${arguments}";
		echo "***************************"
		showHelpMsg;

	else
		case $1 in
		 	-u | --up)
				startMySQLContainer;
		 		;;
		 	-d | --down)
				stopMySQLContainer;
				;;
			-r | --restart)
				stopMySQLContainer;
				startMySQLContainer;
				;;
			-s | --status)
				statusMySQLContainer;
				;;
			-h | --help)
				showHelpMsg;
				;;
			*)
				echo -e "$1: unknown option";
				showHelpMsg;
		esac 
	fi
}

#main entry point of the program
main $@