package com.example.liquibase.migration;

import java.sql.SQLException;

import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author Sumitabha Chandra<br>
 * Server Mode enables multiple user access to the same database.
 * In embedded mode if api is up and running then only one user
 * can access database at a time. 
 *
 */

@Configuration
public class H2DatabaseServerModeConfig {
	
	@Bean(initMethod = "start", destroyMethod = "stop")
	public Server h2Server() throws SQLException{
		return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "9092");
	}

}
