package com.example.liquibase.migration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import liquibase.integration.spring.SpringLiquibase;

@SpringBootApplication
public class LiquibaseMigrationDemoApplication implements CommandLineRunner{
	
	Logger logger = LoggerFactory.getLogger(LiquibaseMigrationDemoApplication.class);
	
	@Value("${spring.profiles.active}")
	private String activeProfile;
	
	@Autowired(required = false)
	SpringLiquibase springLiquibase;
	
	public static void main(String[] args) {
		SpringApplication.run(LiquibaseMigrationDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("\n")
		  .append(activeProfile)
		  .append(" => ")
		  .append("H2: Up & Running in server mode (Multiuser asscess)")
		  .append(" | ")
		  .append("Liquibase: ");
		if(null != springLiquibase) {
			sb.append("database migration complete!...");
		}else {
			sb.append("database migration disabled!...");
		}
		logger.info(sb.toString());
	}

}
