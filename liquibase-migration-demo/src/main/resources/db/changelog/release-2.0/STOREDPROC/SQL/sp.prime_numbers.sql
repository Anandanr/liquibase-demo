CREATE PROCEDURE prime_numbers(IN upper int, OUT result varchar(1000))
BEGIN
	DECLARE mx int;
    DECLARE i int;
    DECLARE j int;
    DECLARE prime boolean;
    SET i=1;
    WHILE i <= upper DO
		SET j=2;
        SET prime = true;
        IF i=1 THEN
			SET result=i;
            SET i=i+1;
		ELSE 
			SET mx=CEIL(SQRT(i));
            WHILE j<=mx DO
				IF (i%j)=0 THEN
					SET prime=false;
				END IF;
				SET j=j+1;
			END WHILE;
		END IF;
		IF prime THEN 
            SET result = concat(result,',', i);
        END IF;
		SET i=i+1;
	END WHILE; 
END;
