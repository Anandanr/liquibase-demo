CREATE FUNCTION `last_nth_digit`(n integer, nth integer) 
RETURNS integer
BEGIN
	DECLARE position  integer;
    DECLARE digit  integer;
    DECLARE nth_digit  integer;
        
    SET position=0;
    SET nth_digit=0;
    
    WHILE position != nth DO
		SET digit = MOD(n, 10);
        SET nth_digit = POWER(10,position)*digit + nth_digit;
        SET n=n DIV 10;
        SET position = position+1;
    END WHILE;
    RETURN nth_digit;
END
//
