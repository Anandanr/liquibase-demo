CREATE TRIGGER before_user_update 
    BEFORE UPDATE ON user
    FOR EACH ROW
	BEGIN
	  SET NEW.updated_at = CURRENT_TIMESTAMP;
	END
//

