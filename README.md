# LIQUIBASE DEMO
[Liquibase](https://www.liquibase.org/) is an open-source, java based, database independent library for tracking, managing and applying
database achema cahnges.

## TABLE OF CONTENTS
- [API DETAILS](#api-details)
- [LIQUIBASE WORK FLOW DIAGRAM](#liquibase-work-flow-diagram)
- [LIQUIBASE INTERNALS](#liquibase-internals)
- [CHANGE LOG FILE ORGANIZATION](#change-log-file-organization)
- [STORY OF EVALUATION](#story-of-evaluation)
  - [RELEASE 1.0](#release-10)
  - [RELEASE 2.0](#release-20)
- [CONCLUSION](#conclusion)
- [REFERENCES](#references)

### API DETAILS
---
1. Checkout or download project
2. If you are using H2 database configure according below details.
3. If you are using MySQL database, configure all database connection path accordingly.  
   You can use provided docker facility, but make sure, docker and docker-compose is installed in your system.
4. Liquibase is configured for two databases - __H2__ and __MySQL__
    - __H2 database:__ 
        1. Datebase file path, has to be configured.  
            - Configure project base directory location, to create dtabase file under project base directory only.  
               - property: `prj.base.url` in application.yml
            - Configure database connection url in : `resources/liquibase/H2/\<profile-name\>/liquibase.properties`
        2. Configured as server mode for multiuser access .
        3. __Application must be up and running to access H2 instance.__
        4. Access details: 

            | ITEM | DESCRIPTION |
            |------| ----------- |
            | H2 console | http://localhost:8080/liquibase-demo/h2-console |
            | database url  |  jdbc:h2:tcp:localhost/~/Projects/LiquibaseWorkspace/liquibase-demo/liquibase-migration-demo/schema/local/db-migration-demo |
            | User Name | sa |
            | Password | Please refer config files |
    - __MySQL database:__
        1. Make sure databsae connection parameters are configured properly as per setup.
        2. You can use MySQL docker container belongs to this project as component.  
           __Usage:__ `./mysql-container [-u|-up ; -d|--down ; -r|--restart ; -s|--status ; -h|--help]`
           ![mysql-docker-container](/static/doc/image/mysql-docker-container-help.png)
5. Migration task can be accomplished in following ways:  
    - [x] Executing maven build, with liquibase plugin and goals(recomended)  
        - Maven build with liquibase plugin goal(s) examples:
            - `mvn liquibase:update -P<maven profile name>`
            - `mvn liquibase:update -P<maven profile name>`
            - `mvn liquibase:update -P<maven profile name> -Dliquibase.contexts=<context name>`
            - `mvn liquibase:rollback -P<maven profile name> -Dliquibase.rollbackTag=create-tg`  
        - __CONFIGURED MAVEN PROFILES:__
            - local(default)
            - dev
            - qa
            - local-mysql  
        - [__LIQUIBASE CONTEXT:__](https://www.liquibase.org/documentation/contexts.html)  
        Liquibase context name could be anything, but often has similarity with profile name(s)/available environment name(s) 
            - local
    - [x] During start up of Spring Boot application(default - disabled)


### LIQUIBASE WORK FLOW DIAGRAM
***
![LIQIBASE WORK FLOW DIAGRAM](/static/doc/image/liquibase-flow-diagram.png)

### LIQUIBASE INTERNALS
***
- Liquibase keeps track of all migration details using below indicated tables in screenshot.
- It keeps hash value of change log file(s) and try to match it in each run to verify databse integrity and update discrepancies.
- **LOCK table is being used to synchronize database migration by multiple instance of liquibase.
- If migration process interrupts, there is possibility of holded lock, not get released, in such case, lock need to be released manually by clearing flag of **LOCK table.

![LIQIBASE INTERNAL](/static/doc/image/liquibase-internal-table.png)

### CHANGE LOG FILE ORGANIZATION
____
![CHANGE LOG FILE ORGANIZATION](/static/doc/image/changelog-file-org.png)

### STORY OF EVALUATION
---
SmartUp, a startup, where You are CEO!, grabs a project to build an eye candy, eye friendly and mobile first web application development.
Application will be based on microservice architecture so that future cloud enablement becomes easy. Development happens in agile mode.
Requirements will be cleared per release basis by the product owner, the client. The development team migrates applicaton database by 
__Liquibase__ as it provides verious features for database migration easy and effective manner.  
Following are the the release wise requirements came to development team, and performed migration activities sequencially in phases, top-down manner.

#### RELEASE 1.0
This migration phase decribes most common types of update using DDL(CREATE, ALTER) and DML(INSERT) and profile specific updates, like, test data inclusion only in local profile.
Underlying database(s) - __H2__ and __MySQL__.
   ##### RESULTANT SCHEMA
   ![release-1.0 schema diagram](/static/doc/image/rls-1.0-schema.png)
   
   ##### TABLE STATE AFTER MIGRATION
   ![release-1.0 migration result](/static/doc/image/release-1.0-mrg-result.png)
   ![release-1.0 migration value](/static/doc/image/release-1.0-mrg-values.png)
   
   ##### CHANGELOG FILES
   ![release-1.0 changelog files](/static/doc/image/rls-1.0-changelog-files.png)
   
   ##### TRANSITION PHASES
   1. Create tables __"user"__ and __"movie"__ with following details 
      :point_right: ![create-table-changelog-1.xml](../../../liquibase-migration-demo/src/main/resources/db/changelog/release-1.0/DDL/create-table-changelog-1.xml) 
      ![update-details-1](/static/doc/image/update-details-1.png)
   2. Alter tables __user__ and __movie__ with following details 
      :point_right: ![alter-table-changelog-1.xml](../../../liquibase-migration-demo/src/main/resources/db/changelog/release-1.0/DDL/alter-table-changelog-1.xml) 
      ![update-details-2](/static/doc/image/update-details-2.png)
   3. Alter tables __user__ and __movie__ with following details 
      :point_right: ![alter-table-changelog-2.xml](../../../liquibase-migration-demo/src/main/resources/db/changelog/release-1.0/DDL/alter-table-changelog-2.xml) 
      ![update-details-3](/static/doc/image/update-details-3.png)
   4. Alter tables __user__ and __movie__ with following details 
      :point_right: ![alter-table-changelog-3.xml](../../../liquibase-migration-demo/src/main/resources/db/changelog/release-1.0/DDL/alter-table-changelog-3.xml) 
      ![update-details-3](/static/doc/image/update-details-4.png)
   5. Create table __watch_list__ with following details 
      :point_right: ![create-table-changelog-2.xml](../../../liquibase-migration-demo/src/main/resources/db/changelog/release-1.0/DDL/create-table-changelog-2.xml) 
      ![update-details-3](/static/doc/image/update-details-5.png)
   4. Alter table __watch_list__ with following details 
      :point_right: ![alter-table-changelog-4.xml](../../../liquibase-migration-demo/src/main/resources/db/changelog/release-1.0/DDL/alter-table-changelog-4.xml) 
      ![update-details-3](/static/doc/image/update-details-6.png)	
   4. Load the initial test data in __local__ environment/profile, but not any higher environment/profile. 
      :point_right: ![insert-table-changelog-2.xml](../../../liquibase-migration-demo/src/main/resources/db/changelog/release-1.0/DML/insert-table-changelog-2.xml) 
   5. During migration maintain checkpoints in various stages for future rollback.

#### RELEASE 2.0   
This migration phase describes how manage __triggers__, __function__ and __store procedure__ using Liquibase. Underlying database(s) - __MySQL__ only.

   ##### CHANGELOG FILES
   ![release-2.0 changelog files](/static/doc/image/rls-2.0-changelog-files.png)
   
   ##### TRANSITION PHASES
   1. Alter all tables, with following details
      :point_right: ![alter-table-changelog-1.xml](../../../liquibase-migration-demo/src/main/resources/db/changelog/release-2.0/DDL/alter-table-changelog-1.xml) 
      ![rls-2.0-update-details-1](/static/doc/image/rls-2.0-update-details-1.png)    

   2. Create triggers, with following details, to log row update timestamp stores at __updated_at__.
      :point_right: ![create-trigger-changelog-1.xml](../../../liquibase-migration-demo/src/main/resources/db/changelog/release-2.0/DDL/create-trigger-changelog-1.xml)  
      ![rls-2.0-update-details-2](/static/doc/image/rls-2.0-update-details-2.png)  
      :thinking: [__Trigger__](https://www.liquibase.org/documentation/changes/create_trigger.html) tag is part of the __Liquibase pro__ feature.

   3. Create stored procedure, with following details
      :point_right: ![storedproc-changelog-1.xml](../../../liquibase-migration-demo/src/main/resources/db/changelog/release-2.0/STOREDPROC/storedproc-changelog-1.xml) 
      ![rls-2.0-update-details-3](/static/doc/image/rls-2.0-update-details-3.png)
      - __greeter:__ Displays greeting message.
        ![strd-prc-greeter](/static/doc/image/strd-prc-greeter.png)
      - __prime_numbers:__ Displays all prime numbers in provided range.
        ![strd-prc-prime-numbers](/static/doc/image/strd-prc-prime-numbers.png)

   4. Create stored function with following details 
      :point_right: ![function-changelog-1.xml](../../../liquibase-migration-demo/src/main/resources/db/changelog/release-2.0/FUNCTION/function-changelog-1.xml) 
      ![rls-2.0-update-details-4](/static/doc/image/rls-2.0-update-details-4.png)
       - __last_nth_digit:__ Displays last nth digit of given number, where nth is the parameter.
       ![func-last-nth-digit](/static/doc/image/func-last-nth-digit.png)  
       :thinking: [__Stored function__](https://www.liquibase.org/documentation/changes/create_function.html) tag is part of the __Liquibase pro__ feature.
    

#### CONCLUSION
Liquibase is the most popular opensource, java beased database migration tools among database user/admin. Here is little try to 
scratch the surface, adhering best practices. Lots of other database objects(
[sequence](https://www.liquibase.org/documentation/changes/create_sequence.html), 
[index](https://www.liquibase.org/documentation/changes/create_index.html) and 
[views](https://www.liquibase.org/documentation/changes/create_view.html) etc.)
can also be created using Liquibase privided xml tag. But not all Liquibase xml 
tags([package](https://www.liquibase.org/documentation/changes/create_package.html), 
[function](https://www.liquibase.org/documentation/changes/create_function.html), 
[trigger](https://www.liquibase.org/documentation/changes/create_trigger.html) and 
[synonym](https://www.liquibase.org/documentation/changes/create_synonym.html) etc.) is included in free version, fews are part of the __Liquibase pro__ feature,
but user can always adopt sql file execution approach to create those database object(s) in pure SQL way, instead of going __Liquibase pro__ way.

### REFERENCES

- [Liquibase](https://www.liquibase.org/)